module StreamingWiki
  class Differences
    class NoopCommit
      def commit(*)

      end
    end

    def self.in(base)
      adds = base.status.untracked.merge(base.status.changed)
      removes = base.status.deleted

      return NoopCommit.new if adds.empty? && removes.empty?

      adds.each do |path, diff|
        base.add(path)
      end

      removes.each do |path, diff|
        base.remove(path)
      end

      base
    end
  end
end
