require "pathname"
require "thor"
require "listen"
require "git"

require "pry"

require "streaming_wiki/differences"

module StreamingWiki
  class Cli < Thor

    desc "live [PATH]", "watches for changes and pushes ./wiki or PATH"
    def live(path="./wiki")
      logger = Logger.new(STDOUT)
      git = Git.open(path, log: logger)
      listen = Listen.to(path) do |modified, added, removed|
        Differences.in(git).commit("notes")
        git.push
      end
      listen.start
      sleep
    rescue
      binding.pry
    end
  end
end
