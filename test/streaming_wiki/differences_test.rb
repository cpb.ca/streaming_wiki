require "test_helper"
require "streaming_wiki/differences"

require "git"
require "listen"

class TestDifferences < FileInteractions
  describe StreamingWiki::Differences,".in git" do
    let(:differences) { StreamingWiki::Differences.in(git_client.open(path)) }

    module MustBeCommitable
      define_method "test_must not raise error if committed" do
        begin
          differences.commit("updates")
          assert true
        rescue Exception => e
          assert false, "Expected to be able to commit but got: #{e}"
        end
      end
    end

    describe "with no changes" do
      include MustBeCommitable
      before do
        base = git_client.open(path)
        create_file("home.md",<<-end)
        * [some issue](1-some-issue)
        end
        create_file("1-some-issue.md",<<-end)
        foobar
        end
        base.add("home.md")
        base.add("1-some-issue.md")
        base.commit("initial commit")
      end
    end

    describe "with tracked files removed" do
      include MustBeCommitable
      before do
        base = git_client.open(path)
        create_file("home.md",<<-end)
        * [some issue](1-some-issue)
        end
        create_file("1-some-issue.md",<<-end)
        foobar
        end
        base.add("home.md")
        base.add("1-some-issue.md")
        base.commit("initial commit")

        path.join("1-some-issue.md").unlink
      end

      it "returns an index with the removed file ready to commit" do
        value(differences.lib.diff_files).must_be_empty
      end
    end

    describe "with tracked, modified files" do
      include MustBeCommitable
      before do
        base = git_client.open(path)
        create_file("home.md",<<-END)
        * [Some Issue](1-some-issue)
        END

        base.add("home.md")
        base.commit("initial commit")

        create_file("home.md",<<-END)
        # Issues
        * #1 [Crashes when file saved without changes - `1-crashes-file-save`](1-crashes-file-save)

        # Features
        END
      end

      it "returns an index with modified file ready to commit" do
        value(differences.lib.diff_files).must_be_empty
      end
    end

    describe "with new untracked files" do
      include MustBeCommitable
      before do
        base = git_client.open(path)
        create_file("home.md",<<-END)
        * [Some Issue](1-some-issue)
        END
        base.add("home.md")
        base.commit("initial commit")

        create_file("1-some-issue.md",<<-END)
        # Kaizen
        - [ ] foobar
        END
      end

      it "returns an index with the new file ready to commit" do
        value(differences.status.untracked).must_be_empty
      end
    end
  end
end
