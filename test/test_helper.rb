$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "pry"

require "streaming_wiki"

require "minitest/autorun"

class GitChainable < BasicObject
  def initialize(git)
    @git = git
  end

  def log(logger)
    @log = logger
    self
  end

  def method_missing(name, *args, **kargs, &block)
    @git.send(name,*args,**kargs,&block)
  end

  def open(path)
    @git.open(path, log: @log)
  end
end

class FileInteractions < Minitest::Spec
  let(:git_client) { GitChainable.new(Git).log(Logger.new(STDOUT)) }
  let(:listen_client) { Struct.new(:to).new }

  def path
    @unit_git_path
  end

  def setup
    @unit_git_path = Pathname.new(__FILE__).dirname + "files" + self.class.name + name
    @unit_git_path.rmtree if @unit_git_path.exist?
    @unit_git_path.mkpath
    git_client.init(path.to_s)
  end

  def teardown
    if failures.empty?
      @unit_git_path.rmtree
      @unit_git_path.dirname.rmdir rescue nil
    end
  end

  def create_file(name,content)
    (path+name).open("w") do |file|
      file.write(content)
    end
  end
end
