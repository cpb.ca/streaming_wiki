require_relative 'lib/streaming_wiki/version'

Gem::Specification.new do |spec|
  spec.name          = "streaming_wiki"
  spec.version       = StreamingWiki::VERSION
  spec.authors       = ["Caleb Buxton"]
  spec.email         = ["me+streaming_wiki@cpb.ca"]

  spec.summary       = %q{Streams changes to a remote git wiki}
  spec.description   = %q{Watches a markdown git wiki for changes, automatically commits and pushes.}
  spec.homepage      = "https://gitlab.com/cpb.ca/streaming_wiki"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "git", "~> 1.7.0"
  spec.add_dependency "listen", "~> 3.1.5"
  spec.add_dependency "thor", "~> 1.0.1"
end
